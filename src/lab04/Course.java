package lab04;

public class Course {
    private String department;
    private int code;

    public Course(String department, int code) {
        this.department =  department;
        this.code = code;
    }
}
